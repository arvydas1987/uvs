﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UVS.Models;

namespace UVS.Services
{
    class InfoChanger
    {
        public delegate void NewLineInformerDelegate(GeneratedData generatedData);
        
        public event NewLineInformerDelegate NewLineInformerEvent;

        private static InfoChanger infoChanger;

        private InfoChanger() { }
        
        public static InfoChanger Instance()
        {
            if (infoChanger == null)
            {
                infoChanger = new InfoChanger();
            }

            return infoChanger;
        }

        public void CallEvent(GeneratedData generatedData)
        {
            NewLineInformerEvent(generatedData);
        }
    }
}
