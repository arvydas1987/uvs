﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using UVS.Models;

namespace UVS.Services
{
    class GeneratedDataDB
    {
        public void InsertData(GeneratedData generatedData)
        {
            using (SqlConnection connection = new SqlConnection("Server = localhost; Database = UVS; Trusted_Connection = True"))
            {
                connection.Open();
                string sqlQuery = string.Format("INSERT INTO Threads ( ThreadId, GeneratedTime, GeneratedLine)" +
                                             "values ({0}, \'{1}\', {2});", generatedData.ThreadId, generatedData.GeneratedTime, generatedData.SymbolsLine);

                SqlCommand command = new SqlCommand(sqlQuery, connection);  
                command.ExecuteNonQuery();
            }
        }
    }
}
