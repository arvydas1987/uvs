﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using UVS.Models;

namespace UVS.Services
{
    class RunThreads
    {
        private static RunThreads runThreads;
        private List<Thread> threads;
       
        private RunThreads() { }

        public static RunThreads Instance()
        {
            if (runThreads == null)
            {
                runThreads = new RunThreads();
            }

            return runThreads;
        }

        public void Start(int threadsCount)
        {
            threads = new List<Thread>();
            for (int i = 1; i <= threadsCount; i++)
            {
                Thread thread = new Thread(NewOne);
                thread.Start(i);
                threads.Add(thread);
            }
        }
        
        private void NewOne(object threadId)
        {
            Random random = new Random();
            GeneratedDataDB generatedDataDB = new GeneratedDataDB();

            while (true)
            {
                GeneratedData generatedData = new GeneratedData
                {
                    ThreadId = (int)threadId,
                    SymbolsLine = GenerateSymbols(),
                    GeneratedTime = DateTime.Now
                };
                generatedDataDB.InsertData(generatedData);
                InfoChanger.Instance().CallEvent(generatedData);
                
                int miliseconds = random.Next(500, 2000);
                Thread.Sleep(miliseconds);
            }
            
        }

        private string GenerateSymbols()
        {
            Random random = new Random();
            int lineCount = random.Next(5, 10);
            string line = "";

            for (int i = 0; i < lineCount; i++ )
            {
                line += i.ToString();
            }

            return line;
        }

        public void Stop()
        {
            foreach (Thread item in threads)
            {
                item.Abort();
            }     
        }
    }
}
