﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using UVS.Services;
using UVS.Models;

namespace UVS
{
    public partial class Form1 : Form
    {
        private object threadLock = new object();

        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            button2.Enabled = false;
            InfoChanger.Instance().NewLineInformerEvent += NewLineGeneratedEvent;
        }

        private void NewLineGeneratedEvent(GeneratedData generatedData)
        {
            lock (threadLock)
            {
                //if (listView1)
                if (listView1.Items.Count > 20)
                {
                    //listView1;
                    listView1.Items[0].Remove();
                }

                string[] row = { generatedData.ThreadId.ToString(), generatedData.SymbolsLine };
                ListViewItem listViewItem = new ListViewItem(row);
                //listViewItem.SubItems.Add(generatedData.ThreadId.tos);
                //listViewItem.SubItems.Add(generatedData.SymbolsLine);
                listView1.Items.Add(listViewItem);
            }
            //listView1.Items.Add(generatedData.SymbolsLine);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int threadsCount = 0;

            if (int.TryParse(textBox1.Text, out threadsCount))
            {
                if ((threadsCount >= 2) && (threadsCount <= 15))
                {
                    button1.Enabled = false;
                    button2.Enabled = true;
                    RunThreads.Instance().Start(threadsCount);
                }
                else
                {
                    MessageBox.Show("Value must be integer type number between 2-15");
                }
            }
            else
            {
                MessageBox.Show("Value must be integer type number between 2-15");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RunThreads.Instance().Stop();
            button1.Enabled = true;
            button2.Enabled = false;
        }

    }
}
