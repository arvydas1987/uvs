﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UVS.Models
{
    class GeneratedData
    {
        public int ThreadId { get; set; }
        public string SymbolsLine { get; set; }
        public DateTime GeneratedTime { get; set; }
    }
}
